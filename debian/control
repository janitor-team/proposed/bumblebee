Source: bumblebee
Section: utils
Priority: optional
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders:
 Aron Xu <aron@debian.org>,
 Yunqiang Su <wzssyqa@gmail.com>,
 Luca Boccassi <bluca@debian.org>,
 Andreas Beckmann <anbe@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 help2man,
 libbsd-dev,
 libglib2.0-dev,
 libx11-dev,
 pkg-config,
 libkmod-dev,
 bash-completion,
Rules-Requires-Root: no
Standards-Version: 4.5.1
Homepage: https://launchpad.net/~bumblebee
Vcs-Browser: https://salsa.debian.org/nvidia-team/bumblebee
Vcs-Git: https://salsa.debian.org/nvidia-team/bumblebee.git

Package: bumblebee
Architecture: linux-any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 lsb-base,
 xserver-xorg-core (>= 2:1.18),
 ${bumblebee:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 bbswitch-dkms | bbswitch-source,
 ${bumblebee:backend},
Suggests:
 bumblebee-nvidia (= ${binary:Version}),
Description: NVIDIA Optimus support for Linux
 Bumblebee is an effort to make NVIDIA Optimus enabled laptops work in
 GNU/Linux systems. These laptops are built in such a way that the NVIDIA
 graphics card can be used on demand so that battery life is improved and
 temperature is kept low.
 .
 It disables the discrete graphics card if no client is detected, and starts
 an X server making use of NVIDIA card if requested then let software GL
 implementations (such as VirtualGL) copy frames to the visible display that
 runs on the integrated graphics. The ability to use discrete graphics
 depends on the driver: open source nouveau and proprietary nvidia.

Package: bumblebee-nvidia
Architecture: amd64 i386 armhf arm64 ppc64el
Section: contrib/utils
Depends:
 bumblebee (= ${binary:Version}),
 ${misc:Depends},
 ${nvidia:Depends},
Recommends:
 nvidia-primus-vk-wrapper,
Description: NVIDIA Optimus support using the proprietary NVIDIA driver
 This metapackage ensures that the proprietary NVIDIA driver is installed in a
 way such that 3D acceleration does not break. It does so by configuring the
 OpenGL library path to use the Mesa graphics library.
